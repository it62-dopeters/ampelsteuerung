﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace TrafficLightControl
{
    public class TrafficLight // By Dominik Peters
    {
        public int TrafficLight_id_in_db { get; set; }

        public Panel Panel_green { get; set; }
        public Panel Panel_orange { get; set; }
        public Panel Panel_red { get; set; }

        public TrafficLightStates Current_trafficLight_state { get; set; }// = TrafficLightStates.ALL_OFF;

        private Timer Timer;

        public void SetTrafficLight_id_in_db(int value) => TrafficLight_id_in_db = value;

        public enum TrafficLightColors
        {
            GRUEN_ON,
            GRUEN_OFF,
            ORANGE_ON,
            ORANGE_OFF,
            RED_ON,
            RED_OFF
        }

        public enum TrafficLightStates
        {
            ALL_OFF,
            GREEN,
            ORANGE,
            RED,
            RED_ORANGE
        }

        public static Dictionary<TrafficLightColors, Color> trafficLight_colors = new Dictionary<TrafficLightColors, Color>()
        {
            { TrafficLightColors.GRUEN_ON, Color.Lime },
            { TrafficLightColors.GRUEN_OFF, Color.FromArgb(0, 64, 0) },
            { TrafficLightColors.ORANGE_ON, Color.Orange },
            { TrafficLightColors.ORANGE_OFF, Color.FromArgb(192, 64, 0) },
            { TrafficLightColors.RED_ON, Color.Red },
            { TrafficLightColors.RED_OFF, Color.FromArgb(192, 0, 0) },
        };

        public static Dictionary<TrafficLightStates, bool[]> trafficLight_states = new Dictionary<TrafficLightStates, bool[]>()
        {
            // TrafficLight States = Green, Orange, Red
            { TrafficLightStates.ALL_OFF, new bool[3]{ false, false, false} },
            { TrafficLightStates.GREEN, new bool[3]{ true, false, false } },
            { TrafficLightStates.ORANGE, new bool[3]{ false, true, false } },
            { TrafficLightStates.RED, new bool[3]{ false, false, true } },
            { TrafficLightStates.RED_ORANGE, new bool[3] { false, true, true } }
        };

        public TrafficLight(Panel panel_green, Panel panel_orange, Panel panel_red)
        {
            Panel_green = panel_green;
            Panel_orange = panel_orange;
            Panel_red = panel_red;
        }

        private void PhaseTrafficLight(object source, EventArgs e)
        {
            TrafficLight.TrafficLightStates previousState = GetCurrentState();
            ColorTrafficLightPanels();
            SetNextState();
            TrafficLightStates currentState = GetCurrentState();
            
            if (IsLastTrafficLightState(previousState, currentState))
            {
                DeactivateTimer();
                return;
            }
        }

        public void SetTimer(Timer timer)
        {
            Timer = timer;
            Timer.Tick += PhaseTrafficLight;
        }

        public void ActivateTimer(int interval)
        {

            if (GetCurrentState() == TrafficLightStates.ALL_OFF)
            {
                SetNextState();
            }
            Timer.Interval = interval;
            Timer.Enabled = true;
        }

        public void ActivateTimer() => Timer.Enabled = true;

        public void DeactivateTimer() => Timer.Enabled = false;

        public TrafficLightStates GetCurrentState() => Current_trafficLight_state;

        public void SetCurrentState(TrafficLightStates new_state) => Current_trafficLight_state = new_state;

        public TrafficLightStates GetNextState()
        {
            TrafficLightStates next_state;
            switch (Current_trafficLight_state)
            {
                case TrafficLightStates.ALL_OFF:
                    next_state = TrafficLightStates.GREEN;
                    break;
                case TrafficLightStates.GREEN:
                    next_state = TrafficLightStates.ORANGE;
                    break;
                case TrafficLightStates.ORANGE:
                    next_state = TrafficLightStates.RED;
                    break;
                case TrafficLightStates.RED:
                    next_state = TrafficLightStates.RED_ORANGE;
                    break;
                case TrafficLightStates.RED_ORANGE:
                    next_state = TrafficLightStates.ALL_OFF;
                    break;
                default:
                    next_state = TrafficLightStates.ALL_OFF;
                    break;
            }
            return next_state;
        }

        public void SetNextState()
        {
            TrafficLightStates next_state = GetNextState();
            SetCurrentState(next_state);
        }

        public void ColorTrafficLightPanels()
        {
            bool[] states = trafficLight_states[GetCurrentState()];
            // Gruene TrafficLight
            if (states[0])
            {
                Set_BackColor(Panel_green, trafficLight_colors[TrafficLightColors.GRUEN_ON]);
            }
            else
            {
                Set_BackColor(Panel_green, trafficLight_colors[TrafficLightColors.GRUEN_OFF]);
            }

            // Orange TrafficLight
            if (states[1])
            {
                Set_BackColor(Panel_orange, trafficLight_colors[TrafficLightColors.ORANGE_ON]);
            }
            else
            {
                Set_BackColor(Panel_orange, trafficLight_colors[TrafficLightColors.ORANGE_OFF]);
            }

            // Red TrafficLight
            if (states[2])
            {
                Set_BackColor(Panel_red, trafficLight_colors[TrafficLightColors.RED_ON]);
            }
            else
            {
                Set_BackColor(Panel_red, trafficLight_colors[TrafficLightColors.RED_OFF]);
            }

        }

        private void Set_BackColor(Panel panel, Color color)
        {
            panel.BackColor = color;
        }


        public bool IsLastTrafficLightState(TrafficLight.TrafficLightStates previous_state, TrafficLight.TrafficLightStates current_state)
        {
            return (current_state == TrafficLight.TrafficLightStates.GREEN && previous_state == TrafficLight.TrafficLightStates.ALL_OFF);
        }

        public void IncrementTrafficLightPresses()
        {
            if (Convert.ToInt32(TrafficLight_id_in_db) == 0) { return; }
            DatabaseHelper dbHelper = new DatabaseHelper();
            dbHelper.Query("UPDATE ampel SET presses = presses+1 WHERE ampelID=" + TrafficLight_id_in_db);
        }

        public int GetTrafficLightPresses()
        {
            if (Convert.ToInt32(TrafficLight_id_in_db) == 0) { return 0; }
            DatabaseHelper dbHelper = new DatabaseHelper();
            List<string[]> entries = dbHelper.Select("SELECT presses FROM ampel WHERE ampelID=" + TrafficLight_id_in_db);
            if (entries.Count == 1)
            {
                return Convert.ToInt32(entries[0][0]);
            }
            else
            {
                return 0;
            }
        }
    }
}

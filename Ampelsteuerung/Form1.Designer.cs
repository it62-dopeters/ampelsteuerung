﻿namespace TrafficLightControl
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.trafficLight_1_orange = new System.Windows.Forms.Panel();
            this.trafficLight_2_orange = new System.Windows.Forms.Panel();
            this.label_trafficLight2 = new System.Windows.Forms.Label();
            this.label_trafficLight_2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_taster_value = new System.Windows.Forms.Label();
            this.timerWarrior = new System.Windows.Forms.Timer(this.components);
            this.chkTasterS1S2 = new System.Windows.Forms.CheckBox();
            this.chkTasterS3 = new System.Windows.Forms.CheckBox();
            this.timerTrafficLight1 = new System.Windows.Forms.Timer(this.components);
            this.timerTrafficLight2 = new System.Windows.Forms.Timer(this.components);
            this.trafficLight_2_gruen = new System.Windows.Forms.Panel();
            this.trafficLight_1_gruen = new System.Windows.Forms.Panel();
            this.trafficLight_2_red = new System.Windows.Forms.Panel();
            this.trafficLight_1_red = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_trafficLight1_counter = new System.Windows.Forms.Label();
            this.label_trafficLight2_counter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // trafficLight_1_orange
            // 
            this.trafficLight_1_orange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.trafficLight_1_orange.Location = new System.Drawing.Point(12, 68);
            this.trafficLight_1_orange.Name = "trafficLight_1_orange";
            this.trafficLight_1_orange.Size = new System.Drawing.Size(30, 30);
            this.trafficLight_1_orange.TabIndex = 1;
            // 
            // trafficLight_2_orange
            // 
            this.trafficLight_2_orange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.trafficLight_2_orange.Location = new System.Drawing.Point(129, 67);
            this.trafficLight_2_orange.Name = "trafficLight_2_orange";
            this.trafficLight_2_orange.Size = new System.Drawing.Size(30, 30);
            this.trafficLight_2_orange.TabIndex = 4;
            // 
            // label_trafficLight2
            // 
            this.label_trafficLight2.AutoSize = true;
            this.label_trafficLight2.Location = new System.Drawing.Point(126, 13);
            this.label_trafficLight2.Name = "label_trafficLight2";
            this.label_trafficLight2.Size = new System.Drawing.Size(45, 13);
            this.label_trafficLight2.TabIndex = 6;
            this.label_trafficLight2.Text = "Ampel 2";
            // 
            // label_trafficLight_2
            // 
            this.label_trafficLight_2.AutoSize = true;
            this.label_trafficLight_2.Location = new System.Drawing.Point(9, 13);
            this.label_trafficLight_2.Name = "label_trafficLight_2";
            this.label_trafficLight_2.Size = new System.Drawing.Size(45, 13);
            this.label_trafficLight_2.TabIndex = 7;
            this.label_trafficLight_2.Text = "Ampel 1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Aktueller Wert der Taster:";
            this.label1.Visible = false;
            // 
            // label_taster_value
            // 
            this.label_taster_value.AutoSize = true;
            this.label_taster_value.Location = new System.Drawing.Point(143, 201);
            this.label_taster_value.Name = "label_taster_value";
            this.label_taster_value.Size = new System.Drawing.Size(25, 13);
            this.label_taster_value.TabIndex = 9;
            this.label_taster_value.Text = "255";
            this.label_taster_value.Visible = false;
            // 
            // timerWarrior
            // 
            this.timerWarrior.Enabled = true;
            this.timerWarrior.Interval = 1;
            // 
            // chkTasterS1S2
            // 
            this.chkTasterS1S2.AutoSize = true;
            this.chkTasterS1S2.Location = new System.Drawing.Point(12, 226);
            this.chkTasterS1S2.Name = "chkTasterS1S2";
            this.chkTasterS1S2.Size = new System.Drawing.Size(96, 17);
            this.chkTasterS1S2.TabIndex = 10;
            this.chkTasterS1S2.Text = "Taster S1 / S2";
            this.chkTasterS1S2.UseVisualStyleBackColor = true;
            this.chkTasterS1S2.Visible = false;
            this.chkTasterS1S2.CheckedChanged += new System.EventHandler(this.ChkTasterS1S2_CheckedChanged);
            // 
            // chkTasterS3
            // 
            this.chkTasterS3.AutoSize = true;
            this.chkTasterS3.Location = new System.Drawing.Point(12, 250);
            this.chkTasterS3.Name = "chkTasterS3";
            this.chkTasterS3.Size = new System.Drawing.Size(72, 17);
            this.chkTasterS3.TabIndex = 11;
            this.chkTasterS3.Text = "Taster S3";
            this.chkTasterS3.UseVisualStyleBackColor = true;
            this.chkTasterS3.Visible = false;
            this.chkTasterS3.CheckedChanged += new System.EventHandler(this.ChkTasterS3_CheckedChanged);
            // 
            // trafficLight_2_gruen
            // 
            this.trafficLight_2_gruen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.trafficLight_2_gruen.Location = new System.Drawing.Point(129, 104);
            this.trafficLight_2_gruen.Name = "trafficLight_2_gruen";
            this.trafficLight_2_gruen.Size = new System.Drawing.Size(30, 30);
            this.trafficLight_2_gruen.TabIndex = 13;
            // 
            // trafficLight_1_gruen
            // 
            this.trafficLight_1_gruen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.trafficLight_1_gruen.Location = new System.Drawing.Point(12, 104);
            this.trafficLight_1_gruen.Name = "trafficLight_1_gruen";
            this.trafficLight_1_gruen.Size = new System.Drawing.Size(30, 30);
            this.trafficLight_1_gruen.TabIndex = 16;
            // 
            // trafficLight_2_red
            // 
            this.trafficLight_2_red.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.trafficLight_2_red.Location = new System.Drawing.Point(129, 31);
            this.trafficLight_2_red.Name = "trafficLight_2_red";
            this.trafficLight_2_red.Size = new System.Drawing.Size(30, 30);
            this.trafficLight_2_red.TabIndex = 15;
            // 
            // trafficLight_1_red
            // 
            this.trafficLight_1_red.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.trafficLight_1_red.Location = new System.Drawing.Point(12, 32);
            this.trafficLight_1_red.Name = "trafficLight_1_red";
            this.trafficLight_1_red.Size = new System.Drawing.Size(30, 30);
            this.trafficLight_1_red.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Ampel 1 gedrückt:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Ampel 2 gedrückt:";
            // 
            // label_trafficLight1_counter
            // 
            this.label_trafficLight1_counter.AutoSize = true;
            this.label_trafficLight1_counter.Location = new System.Drawing.Point(143, 157);
            this.label_trafficLight1_counter.Name = "label_trafficLight1_counter";
            this.label_trafficLight1_counter.Size = new System.Drawing.Size(13, 13);
            this.label_trafficLight1_counter.TabIndex = 19;
            this.label_trafficLight1_counter.Text = "0";
            // 
            // label_trafficLight2_counter
            // 
            this.label_trafficLight2_counter.AutoSize = true;
            this.label_trafficLight2_counter.Location = new System.Drawing.Point(143, 179);
            this.label_trafficLight2_counter.Name = "label_trafficLight2_counter";
            this.label_trafficLight2_counter.Size = new System.Drawing.Size(13, 13);
            this.label_trafficLight2_counter.TabIndex = 20;
            this.label_trafficLight2_counter.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(251, 279);
            this.Controls.Add(this.label_trafficLight2_counter);
            this.Controls.Add(this.label_trafficLight1_counter);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.trafficLight_1_gruen);
            this.Controls.Add(this.trafficLight_2_red);
            this.Controls.Add(this.trafficLight_1_red);
            this.Controls.Add(this.trafficLight_2_gruen);
            this.Controls.Add(this.chkTasterS3);
            this.Controls.Add(this.chkTasterS1S2);
            this.Controls.Add(this.label_taster_value);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_trafficLight_2);
            this.Controls.Add(this.label_trafficLight2);
            this.Controls.Add(this.trafficLight_2_orange);
            this.Controls.Add(this.trafficLight_1_orange);
            this.Name = "Form1";
            this.Text = "Ampelsteuerung";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel trafficLight_1_orange;
        private System.Windows.Forms.Panel trafficLight_2_orange;
        private System.Windows.Forms.Label label_trafficLight2;
        private System.Windows.Forms.Label label_trafficLight_2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_taster_value;
        private System.Windows.Forms.Timer timerWarrior;
        private System.Windows.Forms.CheckBox chkTasterS1S2;
        private System.Windows.Forms.CheckBox chkTasterS3;
        private System.Windows.Forms.Timer timerTrafficLight1;
        private System.Windows.Forms.Timer timerTrafficLight2;
        private System.Windows.Forms.Panel trafficLight_2_gruen;
        private System.Windows.Forms.Panel trafficLight_1_gruen;
        private System.Windows.Forms.Panel trafficLight_2_red;
        private System.Windows.Forms.Panel trafficLight_1_red;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_trafficLight1_counter;
        private System.Windows.Forms.Label label_trafficLight2_counter;
    }
}


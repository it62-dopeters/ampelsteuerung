﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrafficLightControl
{
    class TrafficLightIOWBridge // By Florian Weiner
    {
        private TrafficLight trafficLight;
        private IntPtr warriorHandle;
        private IOWHelper helper = new IOWHelper();

        Dictionary<TrafficLight.TrafficLightStates, int> stateToValueMap = new Dictionary<TrafficLight.TrafficLightStates, int>()
        {
            { TrafficLight.TrafficLightStates.ALL_OFF, 255 },
            { TrafficLight.TrafficLightStates.GREEN, 254 },
            { TrafficLight.TrafficLightStates.ORANGE, 253 },
            { TrafficLight.TrafficLightStates.RED, 251 },
            { TrafficLight.TrafficLightStates.RED_ORANGE, 249 }
        };

        public TrafficLightIOWBridge(TrafficLight trafficLight, IntPtr warriorHandle)
        {
            this.trafficLight = trafficLight;
            this.warriorHandle = warriorHandle;
            helper.Current_warrior_handle = warriorHandle;
        }
        private bool IsWarriorConnected(){
            return (warriorHandle != IntPtr.Zero);
        }

        public void WriteTrafficLightStateToWarrior()
        {
            if (!IsWarriorConnected())
            {
                return;
            }
            TrafficLight.TrafficLightStates state = trafficLight.GetCurrentState();
            
            helper.Deactivate_All_Ports();
            helper.Activate_Port(1);
            byte[] daten = new byte[4];
            daten[0] = 0;
            daten[1] = Convert.ToByte(stateToValueMap[state]);
            daten[2] = 255;
            //helper.Open_IOW();
            helper.Write_IOW(warriorHandle, daten);
            //helper.Close_IOW();
            
        }
    }
}

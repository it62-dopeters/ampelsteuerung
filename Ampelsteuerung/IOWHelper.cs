﻿using System;
using System.Diagnostics;
using System.Timers;

namespace TrafficLightControl
{
    public class IOWHelper
    {
        private readonly int[] port_values = new int[8] { 1, 2, 4, 8, 16, 32, 64, 128 };
        private bool[] port_booleans = new bool[8];
        private bool Is_inverted { get; set; }
        public IntPtr Current_warrior_handle { get; set; }
        public IOWHelper(bool inverted)
        {
            Is_inverted = inverted;
            for (int i = 0; i < port_booleans.Length; i++)
            {
                port_booleans[i] = !Is_inverted;
            }
        }

        public IOWHelper()
        {
            Is_inverted = false;
            for (int i = 0; i < port_booleans.Length; i++)
            {
                port_booleans[i] = !Is_inverted;
            }
        }

        public bool Activate_Port(int pin)
        {
            if (port_booleans.Length < pin) { return false; }
            if (Is_inverted)
            {
                port_booleans[pin] = true;
            }
            else
            {
                port_booleans[pin] = false;
            }
            return true;
        }

        public bool Deactivate_Port(int pin)
        {
            if (port_booleans.Length < pin) { return false; }
            if (Is_inverted)
            {
                port_booleans[pin] = false;
            }
            else
            {
                port_booleans[pin] = true;
            }
            return true;
        }

        public void Activate_All_Ports()
        {
            for (int i = 0; i < port_booleans.Length; i++)
            {
                if (Is_inverted)
                {
                    port_booleans[i] = true;
                }
                else
                {
                    port_booleans[i] = false;
                }
            }
        }

        public void Deactivate_All_Ports()
        {
            for (int i = 0; i < port_booleans.Length; i++)
            {
                if (Is_inverted)
                {
                    port_booleans[i] = false;
                }
                else
                {
                    port_booleans[i] = true;
                }
            }
        }

        public byte Get_Current_Port_Value()
        {
            int port_value = 0;
            for (int i = 0; i < port_booleans.Length; i++)
            {
                if (port_booleans[i])
                {
                    port_value += port_values[i];
                }
            }
            if (Is_inverted)
            {
                port_value = 255 - port_value;
            }
            //Console.WriteLine("PORT VALUE :" + port_value);
            return Convert.ToByte(port_value);
        }

        public byte[] Read_Current_Port_Value(byte[] buffer)
        {
            uint read = IOWarrior.Functions.IowKitReadNonBlocking(Current_warrior_handle, 0, buffer, 2);
            return new byte[2];
        }

        public byte[] Read_Current_Port_Value(IntPtr iow_handle, byte[] buffer)
        {
            uint read = IOWarrior.Functions.IowKitReadNonBlocking(iow_handle, 0, buffer, Convert.ToUInt32(buffer.Length));
            //uint read = IOWarrior.Functions.IowKitRead(iow_handle, 0, buffer, 2000);

            return new byte[2];
        }

        public IntPtr Open_IOW()
        {
            Current_warrior_handle = IOWarrior.Functions.IowKitOpenDevice();
            return Current_warrior_handle;
        }

        public void Write_IOW(byte[] daten)
        {
            Debug.WriteLine("Schreibe Daten zu Warrior: " + daten[1].ToString());
            IOWarrior.Functions.IowKitWrite(Current_warrior_handle, 0, daten, Convert.ToUInt32(daten.Length));

        }

        public void Write_IOW(IntPtr iow_handle, byte[] daten)
        {
            uint return_value = IOWarrior.Functions.IowKitWrite(iow_handle, 0, daten, Convert.ToUInt32(daten.Length));
        }

        public void Close_IOW(IntPtr iow_handle)
        {
            IOWarrior.Functions.IowKitCloseDevice(iow_handle);
        }

        public void Close_IOW()
        {
            IOWarrior.Functions.IowKitCloseDevice(Current_warrior_handle);
            Current_warrior_handle = new IntPtr();
        }
    }
}

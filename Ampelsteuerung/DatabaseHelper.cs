﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace TrafficLightControl
{
    class DatabaseHelper // By Florian Weiner
    {
        readonly string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=ampelsteuerung;";
        public void Query(string queryString) { 
            try
            {
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(queryString, databaseConnection)
                {
                    CommandTimeout = 60
                };
                databaseConnection.Open();
                commandDatabase.ExecuteNonQuery();
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<string[]> Select(string queryString)
        {
            List<string[]> collection = new List<string[]>();
            try
            {
                MySqlConnection databaseConnection = new MySqlConnection(connectionString);
                MySqlCommand commandDatabase = new MySqlCommand(queryString, databaseConnection)
                {
                    CommandTimeout = 60
                };
                databaseConnection.Open();
                MySqlDataReader reader = commandDatabase.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string[] row = { reader.GetString(0)};
                        collection.Add(row);
                    }
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return collection;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace TrafficLightControl
{
    public partial class Form1 : Form
    {
        static bool enable_debug = false;
        IntPtr meinIOW = IOWarrior.Functions.IowKitOpenDevice();

        TrafficLight trafficLight1;
        TrafficLight trafficLight2;

        TrafficLightIOWBridge bridgeTrafficLight1;
        TrafficLightIOWBridge bridgeTrafficLight2;

        static int defaultIntervalInMs = 500;
        
        public Form1()
        {
            InitializeComponent();
            trafficLight1 = new TrafficLight(trafficLight_1_gruen, trafficLight_1_orange, trafficLight_1_red)
            {
                TrafficLight_id_in_db = 1
            };
            trafficLight1.SetTimer(timerTrafficLight1);
            trafficLight2 = new TrafficLight(trafficLight_2_gruen, trafficLight_2_orange, trafficLight_2_red)
            {
                TrafficLight_id_in_db = 2
            };
            trafficLight2.SetTimer(timerTrafficLight2);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label_trafficLight1_counter.Text = trafficLight1.GetTrafficLightPresses().ToString();
            label_trafficLight2_counter.Text = trafficLight2.GetTrafficLightPresses().ToString();
            
            EventHandler read_iow = new EventHandler(ReadIOW);
            timerWarrior.Tick += read_iow;

            bridgeTrafficLight1 = new TrafficLightIOWBridge(trafficLight1, meinIOW);
            bridgeTrafficLight2 = new TrafficLightIOWBridge(trafficLight2, meinIOW);
        }

        private void ReadIOW(object source, EventArgs e)
        {
            byte[] daten_empfangen = new byte[4];
            if(meinIOW == IntPtr.Zero || enable_debug)
            {
                // Kein Warrior gefunden
                label_taster_value.Text = "WARRIOR NOT FOUND";
                EnableDebuggingOptions();
                return;
            }
            try
            {
                uint return_value = IOWarrior.Functions.IowKitReadNonBlocking(meinIOW, 0, daten_empfangen, Convert.ToUInt32(daten_empfangen.Length));
                if (daten_empfangen[2] != 0)
                {
                    int portValue = (daten_empfangen[2] & 255);
                    label_taster_value.Text = portValue.ToString();

                    bool isTrafficLight1On = (daten_empfangen[2] & 1) == 1;
                    bool isTrafficLight2On = (daten_empfangen[2] & 2) == 2;
                    if (isTrafficLight1On)
                    {
                        trafficLight1.IncrementTrafficLightPresses();
                        label_trafficLight1_counter.Text = trafficLight1.GetTrafficLightPresses().ToString();
                        trafficLight1.ActivateTimer(defaultIntervalInMs);
                    }
                    if (isTrafficLight2On)
                    {
                        trafficLight2.IncrementTrafficLightPresses();
                        label_trafficLight2_counter.Text = trafficLight2.GetTrafficLightPresses().ToString();
                        trafficLight2.ActivateTimer(defaultIntervalInMs);
                    }
                }
            }
            catch(Exception ex)
            {
                // Kein Warrior gefunden
                label_taster_value.Text = "WARRIOR NOT FOUND? " + ex.Message;

            }
        }

        private void EnableDebuggingOptions()
        {
            chkTasterS1S2.Visible = true;
            chkTasterS3.Visible = true;
            label1.Visible = false;
            label_taster_value.Visible = false;

        }

        private void ChkTasterS1S2_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTasterS1S2.Checked)
            {
                if (trafficLight1.GetCurrentState() == TrafficLight.TrafficLightStates.ALL_OFF)
                {
                    trafficLight1.SetNextState();
                }
                trafficLight1.IncrementTrafficLightPresses();
                label_trafficLight1_counter.Text = trafficLight1.GetTrafficLightPresses().ToString();
                trafficLight1.ActivateTimer(defaultIntervalInMs);
            }
            else
            {
                trafficLight1.DeactivateTimer();
            }
        }

        private void ChkTasterS3_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTasterS3.Checked)
            {
                if (trafficLight2.GetCurrentState() == TrafficLight.TrafficLightStates.ALL_OFF)
                {
                    trafficLight2.SetNextState();
                }
                trafficLight2.IncrementTrafficLightPresses();
                label_trafficLight2_counter.Text = trafficLight2.GetTrafficLightPresses().ToString();
                trafficLight2.ActivateTimer(defaultIntervalInMs);
            }
            else
            {
                trafficLight2.DeactivateTimer();
            }
        }
    }
}
